package com.maocq.properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class Main {

	public static void main(String[] args) {

		(new Main()).propiedadesXML();

		// Main main = new Main();
		// main.setPropiedades();
		// main.getPropiedades();
	}

	public void getPropiedades() {

		Properties propiedades = new Properties();
		try (InputStream entrada = new FileInputStream(
				"configuracion.properties")) {

			propiedades.load(entrada);

			System.out.println(propiedades.getProperty("basedatos"));
			System.out.println(propiedades.getProperty("usuario"));
			System.out.println(propiedades.getProperty("clave"));

		} catch (IOException ex) {
			System.out.println("Error: " + ex.getMessage());
		}
	}

	public void setPropiedades() {

		Properties propiedades = new Properties();

		try (OutputStream salida = new FileOutputStream(
				"configuracion.properties")) {

			propiedades.setProperty("basedatos", "dbsql");
			propiedades.setProperty("usuario", "maocq");
			propiedades.setProperty("clave", "123456");

			propiedades.store(salida, null);

		} catch (IOException io) {
			System.out.println("Error: " + io.getMessage());
		}

	}

	public void propiedadesXML() {

		Properties propiedades = new Properties();
		try (InputStream entrada = new FileInputStream("configuracion.xml")) {

			propiedades.loadFromXML(entrada);

			// Imprimir lista de propiedades
			// propiedades.list(System.out);

			System.out.println(propiedades.getProperty("consulta"));

		} catch (IOException ex) {
			System.out.println("Error: " + ex.getMessage());
		}
	}

}
